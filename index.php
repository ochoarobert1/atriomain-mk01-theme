<?php get_header(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <div class="main-title-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="main-title-content col-md-8">
                        <h1 itemprop="headline"><?php _e('Noticias', 'atrio')?></h1>
                    </div>
                    <div class="the-breadcrumbs col-md-4">
                        <?php echo the_breadcrumb(); ?>
                    </div>
                </div>
            </div>   
        </div>
        <section class="news-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 no-paddingl">
                        <?php $defaultatts = array('class' => 'img-responsive'); ?>
                        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                        <article id="post-<?php the_ID(); ?>" class="archive-item col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr <?php echo join(' ', get_post_class()); ?>" role="article">
                            <picture class="archive-item-picture col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <?php if ( has_post_thumbnail()) : ?>
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                    <?php the_post_thumbnail('full', $defaultatts); ?>
                                </a>
                                <?php else : ?>
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/no-img.jpg" alt="No img" class="img-responsive" />
                                </a>
                                <?php endif; ?>
                            </picture>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h2 rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></h2></a>
                                <p><?php the_excerpt(); ?></p>
                                <div class="archive-item-data col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    <span class="author" itemprop="author" itemscope itemptype="http://schema.org/Person">Por: <?php the_author_posts_link(); ?></span>
                                    <span class="date" itemprop="datePublished"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
                                    <span class="more-info"><a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">Más información ></a></span>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </article>
                        <?php endwhile; ?>
                        <div class="pagination col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php if(function_exists('wp_paginate')) { wp_paginate(); } else { posts_nav_link(); } ?>
                        </div>
                    </div>
                    <div class="archive-sidebar col-lg-3 col-md-3 col-sm-3 col-xs-3 no-paddingr">
                       <div class="archive-sidebar-sticky col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                        <?php get_sidebar(); ?>
                        </div>
                    </div>
                    <?php else: ?>
                    <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2>Disculpe, su busqueda no arrojo ningun resultado</h2>
                        <h3>Haga click <a href="<?php echo home_url('/'); ?>">aqui</a> para volver al inicio</h3>
                    </article>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
