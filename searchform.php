<form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>">
    <div class="input-group">
        <input  type="search" id="s" name="s" class="form-control" placeholder="<?php _e('Buscar por Términos:','atrio'); ?>">
        <span class="input-group-btn">
            <button type="submit" id="searchsubmit" class="btn btn-default" type="button"><?php _e('Buscar','atrio'); ?></button>
        </span>
    </div><!-- /input-group -->
</form>