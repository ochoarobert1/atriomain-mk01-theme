<footer class="container-fluid"  role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row">
        <div class="the-footer col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="footer-item col-md-3">
                        <h3 class="footer-title"><?php _e('UBICACIÓN', 'atrio'); ?></h3>
                        <h5>Dirección</h5>
                        <p>Calle Jacinto Mañon, N° 15. Ens. Paraiso.</p>
                        <p>Santo Domingo, República Dominicana.</p>
                        <br />
                        <h5>Teléfonos</h5>
                        <p>+1 (829) 547-5777</p>
                    </div>
                    <div class="footer-item col-md-3">
                        <h3 class="footer-title"><?php _e('NOTICIAS', 'atrio'); ?></h3>
                        <?php $args = array('post_type' => 'post', 'posts_per_page' => 3, 'order' => 'DESC', 'orderby' => 'date'); ?>
                        <?php query_posts($args); ?>
                        <?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post(); ?>
                        <div class="media">
                            <div class="media-left">
                                <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                    <?php the_post_thumbnail('avatar', array('class' => 'media-object')); ?>
                                </a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading"><?php the_title(); ?></h4>
                                <span><?php echo get_the_date(); ?></span>
                            </div>
                        </div>
                        <?php endwhile; ?>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                    <div class="footer-item col-md-3">
                        <h3 class="footer-title"><?php _e('TWITTER', 'atrio'); ?></h3>
                        <?php echo do_shortcode("[rotatingtweets screen_name='AtrioSegurosRD']"); ?>
                    </div>
                    <div class="footer-item col-md-3">
                        <h3 class="footer-title"><?php _e('FACEBOOK', 'atrio'); ?></h3>
                        <?php echo do_shortcode("[custom-facebook-feed]"); ?>
                    </div>

                </div>
            </div>
        </div>
        <div class="the-footer-copyright col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="footer-copy-container col-lg-9 col-md-9 col-sm-9 col-xs-9">
                        <h6>Atrio Seguros© Todos los derechos Reservados</h6>
                        <h6>Desarrollado por <a href="http://robertochoa.com.ve">Robert Ochoa</a></h6>
                    </div>
                    <div class="footer-social col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <a href="https://www.facebook.com/AtrioSegurosRD" title="<?php _e('Visita nuestra Página de Facebook', 'atrio'); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                        <a href="https://twitter.com/AtrioSegurosRD" title="<?php _e('Visita nuestro Perfil de Twiiter', 'atrio'); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                        <a href="https://www.instagram.com/atriosegurosrd/" title="<?php _e('Visita nuestro Perfil de Instagram', 'atrio'); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>

</body>
</html>
