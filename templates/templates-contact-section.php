<?php
/**
 * Template Name: Contact Schematics
 *
 * @package Atrio Main
 * @subpackage atriomain-mk01-theme
 * @since 1.0
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid">
    <div class="row">
        <div class="main-title-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="main-title-content col-md-8">
                        <h1 itemprop="headline"><?php the_title(); ?></h1>
                    </div>
                    <div class="the-breadcrumbs col-md-4">
                        <?php echo the_breadcrumb(); ?>
                    </div>
                </div>
            </div>   
        </div>
        <div class="contact-page-map  col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <?php $args = array(
    'type' => 'map', // Required
    'width' => '100%', // Map width, default is 640px. Can be '%' or 'px'
    'height' => '350px', // Map height, default is 480px. Can be '%' or 'px'
    'zoom' => 17, // Map zoom, default is the value set in admin, and if it's omitted - 14
    'marker' => true, // Display marker? Default is 'true',
    'js_options' => array(
        'zoomControl' => false,
        'zoom' => 17, // You can use 'zoom' inside 'js_options' or as a separated parameter
    ),
); ?>
            <?php $map = rwmb_meta( 'map', $args ); ?>
            <?php echo $map; ?>
        </div>
        <div class="page-container col-lg-12 col-md-12 col-sm-12 col-xs-12" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
            <div class="container">
                <div class="row">
                    <article id="post-<?php the_ID(); ?>" class="page-article col-md-8 <?php echo join(' ', get_post_class()); ?>" itemscope itemtype="http://schema.org/Article">
                        <div class="post-content" itemprop="articleBody">
                            <h3>Contáctanos</h3>
                            <?php $contact_form = get_post_meta(get_the_ID(), 'rw_contact_form', true); ?>
                            <?php echo do_shortcode($contact_form); ?>
                        </div>
                    </article> 

                    <aside class="the-sidebar the-sidebar-contact col-md-4" role="complementary">
                        <?php the_content() ?>
                    </aside>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</main>
<?php get_footer(); ?>
