<?php
/**
 * Template Name: Homepage Schematics
 *
 * @package Atrio Main
 * @subpackage atriomain-mk01-theme
 * @since 1.0
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="the-slider col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <?php $slider = get_post_meta(get_the_ID(), 'rw_slider', true); ?>
            <?php echo do_shortcode('[rev_slider alias="' . $slider . '"]'); ?>
        </section>
        <section class="the-about col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="about-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h1 class="home-title">Te ofrecemos <strong>protección</strong> para</h1>
                        <?php $args = array('post_type' => 'servicios', 'posts_per_page' => 5, 'order' => 'ASC', 'orderby' => 'date', 'meta_query' => array(array('key' => 'rw_service_dest', 'value' => 1, 'compare' => 'LIKE'))); ?>
                        <?php query_posts($args); ?>
                        <?php while (have_posts()) : the_post(); ?>
                        <div class="about-item col-xs-12 col-sm-5ths col-md-5ths col-lg-5ths">
                            <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                <?php $images = rwmb_meta( 'rw_logo_img', 'size=full' );  ?>
                                <?php if ( !empty( $images ) ) { ?>
                                <?php foreach ( $images as $image ) { $full_url = $image['full_url']; } ?>
                                <img class="img-responsive" src="<?php echo $full_url; ?>" alt="<?php echo get_the_title(); ?>" />
                                <?php } ?>
                            </a>
                            <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                <h3><?php the_title(); ?></h3>
                            </a>
                        </div>
                        <?php endwhile; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-container col-md-12" role="article" itemscope itemtype="http://schema.org/BlogPosting">
            <article id="post-<?php the_ID(); ?>" class="page-content <?php echo join(' ', get_post_class()); ?>" >
                <div class="page-article col-md-12 no-paddingl no-paddingr" itemprop="articleBody">
                    <?php the_content(); ?>
                </div>
            </article>
        </section>
    </div>
</main>
<?php get_footer(); ?>
