<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid">
    <div class="row">
        <div class="main-title-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="main-title-content col-md-8">
                        <h1 itemprop="headline"><?php the_title(); ?></h1>
                    </div>
                    <div class="the-breadcrumbs col-md-4">
                        <?php echo the_breadcrumb(); ?>
                    </div>
                </div>
            </div>   
        </div>
        <?php $images = rwmb_meta( 'rw_banner_img', 'size=full' );  ?>
        <?php if ( !empty( $images ) ) { ?>
        <?php foreach ( $images as $image ) { $full_url = $image['full_url']; } ?>
        <div class="main-banner-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr animated fadeIn" style="background: url(<?php echo $full_url; ?>);"></div>
        <?php } ?>
        <div class="single-main-container col-lg-12 col-md-12 col-sm-12 col-xs-12" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
            <div class="container">
                <div class="row">
                    <article id="post-<?php the_ID(); ?>" class="the-single col-md-8 <?php echo join(' ', get_post_class()); ?>" itemscope itemtype="http://schema.org/Article">
                        <div class="post-content" itemprop="articleBody">
                            <?php the_content() ?>
                        </div><!-- .post-content -->
                        <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
                        <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
                        <meta itemprop="url" content="<?php the_permalink() ?>">
                        <?php if ( comments_open() ) { comments_template(); } ?>
                    </article> <?php // end article ?>

                    <aside class="the-sidebar col-md-4" role="complementary">
                        <?php $defaultargs = array('class' => 'img-responsive'); ?>
                        <?php if ( has_post_thumbnail()) : ?>
                        <picture>
                            <?php the_post_thumbnail('full', $defaultargs); ?>
                        </picture>
                        <?php endif; ?>
                    </aside>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</main>
<?php get_footer(); ?>
